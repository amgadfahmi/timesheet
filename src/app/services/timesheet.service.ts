import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment.prod';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

export class Timesheet {
  constructor(
    public employeeId: number,
    public taskId: number,
    public hours: number,
    public taskDate: Date
  ) {}
}
@Injectable()
export class TimesheetService {
  private baseapi = environment.apiUrl;
  constructor(private http: HttpClient) {}

  getTimesheet(employeeId: number, weekNo: number) {
    return this.http.get(
      this.baseapi + `/timesheet/get?employeeId=${employeeId}&weekNo=${weekNo}`
    );
  }

  addHours(timesheet: Timesheet): Observable<Timesheet> {
    return this.http.post<Timesheet>(
      this.baseapi + '/timesheet/add',
      timesheet
    );
  }
}

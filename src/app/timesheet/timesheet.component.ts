import { Component, OnInit } from '@angular/core';
import { TimesheetService, Timesheet } from '../services/timesheet.service';
import { EmployeeService } from '../services/employee.service';
import { ActivatedRoute } from '@angular/router';
import { TaskService } from '../services/task.service';
import * as moment from 'moment';
import { debug } from 'util';

@Component({
  selector: 'app-timesheet',
  templateUrl: './timesheet.component.html',
  styleUrls: ['./timesheet.component.scss']
})
export class TimesheetComponent implements OnInit {
  timesheet: any;
  employees: any;
  tasks: any;
  editMode = false;
  addMode = false;
  selectedEmployee: number;
  selectedWeek: number;
  selectedTask: number;
  selectedDay: Date;
  days = [];

  constructor(
    private route: ActivatedRoute,
    private tsService: TimesheetService,
    private employeeSerice: EmployeeService,
    private taskService: TaskService
  ) {
    this.selectedWeek = 1;
    this.selectedTask = 1;
    this.selectedDay = new Date();
    this.initDays();
  }

  initDays() {
    this.days = [];
    for (let index = 0; index < 7; index++) {
      let daysToSubract = (this.selectedWeek - 1) * 7;
      const dt = moment().subtract(index + daysToSubract, 'days');
      this.days.push({
        id: dt.format(),
        name: dt.format('dddd')
      });
    }
  }

  ngOnInit() {
    this.employeeSerice.getallemployees().subscribe(data => {
      this.employees = data;
      this.getEmployeeId();
      this.getEmployeeSheets(this.selectedEmployee, this.selectedWeek);
    });
    this.taskService.getallTasks().subscribe(data => {
      this.tasks = data;
    });
  }

  next() {
    this.selectedWeek++;
    this.getEmployeeSheets(this.selectedEmployee, this.selectedWeek);
    this.initDays();
  }

  previous() {
    this.selectedWeek--;
    this.getEmployeeSheets(this.selectedEmployee, this.selectedWeek);
    this.initDays();
  }

  private getEmployeeId() {
    this.route.paramMap.subscribe(params => {
      this.selectedEmployee = +params.get('id');
    });
  }

  private getEmployeeSheets(employeeId: number, weekNumber: number) {
    this.tsService.getTimesheet(employeeId, weekNumber).subscribe(data => {
      this.timesheet = data;
    });
  }

  editCell(value: string) {
    this.editMode = !this.editMode;
  }

  employeeChanged(newValue: any) {
    this.selectedEmployee = newValue;
    this.getEmployeeSheets(this.selectedEmployee, this.selectedWeek);
  }

  taskChanged(newValue: any) {
    this.selectedTask = newValue;
  }

  dayChanged(newValue: any) {
    this.selectedDay = newValue;
  }

  addHours(hours: number) {
    const temp = new Timesheet(
      this.selectedEmployee,
      this.selectedTask,
      hours,
      this.selectedDay
    );
    this.tsService.addHours(temp).subscribe(data => {
      this.getEmployeeSheets(this.selectedEmployee, this.selectedWeek);
    });
  }
}

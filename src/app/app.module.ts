import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { Routes, RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { EmployeeListComponent } from './employee/employee.component';
import { TimesheetComponent } from './timesheet/timesheet.component';
import { HomeComponent } from './home/home.component';
import { EmployeeService } from './services/employee.service';
import { TimesheetService } from './services/timesheet.service';
import { TaskService } from './services/task.service';

const routes: Routes = [
  { path: '', redirectTo: '/employees', pathMatch: 'full' },
  { path: 'employees', component: EmployeeListComponent },
  { path: 'timesheet', component: TimesheetComponent },
  { path: 'timesheet/:id', component: TimesheetComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    EmployeeListComponent,
    TimesheetComponent,
    HomeComponent
  ],
  imports: [BrowserModule, HttpClientModule, RouterModule.forRoot(routes)],
  providers: [EmployeeService, TimesheetService, TaskService],
  bootstrap: [AppComponent]
})
export class AppModule {}
